package com.intuit.complaint.management.mongo.models.extensions;

import lombok.Data;

import java.time.Instant;

@Data
public class PurchasesDetailsModel {
    private String id;
    private String userId;
    private String productId;
    private String productName;
    private Double pricePaidAmount;
    private String priceCurrency;
    private Double discountPercent;
    private String merchantId;
    private Instant purchaseDate;
}
