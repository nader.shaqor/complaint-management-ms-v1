package com.intuit.complaint.management.mongo.repository;

import com.intuit.complaint.management.mongo.models.ComplaintModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ComplaintRepository extends MongoRepository<ComplaintModel,String> {

}
