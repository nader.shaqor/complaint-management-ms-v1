package com.intuit.complaint.management.mongo.models.extensions;

import lombok.Data;

@Data
public class UserDetailsModel {
    private String id;
    private String fullName;
    private String emailAddress;
    private String physicalAddress;
}
