package com.intuit.complaint.management.mongo.models;

import com.intuit.complaint.management.mongo.models.extensions.GeneralCollectionModel;
import com.intuit.complaint.management.mongo.models.extensions.PurchasesDetailsModel;
import com.intuit.complaint.management.mongo.models.extensions.UserDetailsModel;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "complaint")
public class ComplaintModel extends GeneralCollectionModel {
    @Id
    private String id;
    @Indexed
    private String userId;
    @Indexed
    private String purchaseId;

    private String subject;
    private String complaint;

    private UserDetailsModel userDetailsData;
    private PurchasesDetailsModel purchasesDetailsData;

}