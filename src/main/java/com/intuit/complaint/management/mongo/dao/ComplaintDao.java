package com.intuit.complaint.management.mongo.dao;

import com.intuit.complaint.management.mongo.models.ComplaintModel;
import com.intuit.complaint.management.mongo.repository.ComplaintRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class ComplaintDao {
    private ComplaintRepository complaintRepository;
    private MongoTemplate mongoTemplate;

    public void insertNewComplaint(ComplaintModel complaintModel){
        complaintRepository.insert(complaintModel);
    }

    public Page<ComplaintModel> getComplaint(String userId, String purchaseId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size,
                Sort.by("createdAt").descending());

        Query query = new Query().with(pageable);

        if (userId != null) {
            query.addCriteria(Criteria.where("userId").is(userId));
        }

        if (purchaseId != null) {
            query.addCriteria(Criteria.where("purchaseId").is(purchaseId));
        }

        List<ComplaintModel> complaintModelList = mongoTemplate.find(query, ComplaintModel.class);
        long count = mongoTemplate.count(query.skip(-1).limit(-1), ComplaintModel.class);

        Page<ComplaintModel> resultPage = new PageImpl<ComplaintModel>(complaintModelList, pageable, count);
        return resultPage;
    }
}
