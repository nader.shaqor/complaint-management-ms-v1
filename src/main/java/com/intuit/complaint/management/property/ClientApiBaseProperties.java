package com.intuit.complaint.management.property;

import lombok.Data;

@Data
public class ClientApiBaseProperties {
    private String apiHost;
    private String apiPath;
}
