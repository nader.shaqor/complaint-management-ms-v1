package com.intuit.complaint.management.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "clients.user")
public class UserClientApiProperties extends ClientApiBaseProperties{
}
