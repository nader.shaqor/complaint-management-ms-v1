package com.intuit.complaint.management.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "clients.purchases")
public class PurchasesClientApiProperties extends ClientApiBaseProperties{
}
