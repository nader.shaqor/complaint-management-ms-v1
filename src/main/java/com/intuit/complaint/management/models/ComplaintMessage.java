package com.intuit.complaint.management.models;

import lombok.Data;

@Data
public class ComplaintMessage {
    private String userId;
    private String subject;
    private String complaint;
    private String purchaseId;
}
