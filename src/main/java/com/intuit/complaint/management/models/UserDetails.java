package com.intuit.complaint.management.models;

import lombok.Data;

@Data
public class UserDetails {
    private String id;
    private String fullName;
    private String emailAddress;
    private String physicalAddress;
}
