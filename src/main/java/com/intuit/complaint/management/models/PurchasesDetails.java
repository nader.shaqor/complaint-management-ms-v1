package com.intuit.complaint.management.models;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class PurchasesDetails {
    private String id;
    private String userId;
    private String productId;
    private String productName;
    private Double pricePaidAmount;
    private String priceCurrency;
    private Double discountPercent;
    private String merchantId;
    private OffsetDateTime purchaseDate;
}
