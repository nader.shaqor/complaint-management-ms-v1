package com.intuit.complaint.management.async.consumers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intuit.complaint.management.models.ComplaintMessage;
import com.intuit.complaint.management.services.interfaces.IComplaintEventHandler;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class CreateComplaintMessageConsumer {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private IComplaintEventHandler iComplaintEventHandler;

    @JmsListener(destination = "${spring.activemq.listeners.destinations.create-complaint}",
            concurrency = "${spring.activemq.listeners.concurrency}")
    public void consumeMessage(String message) {

        log.info("Message received from activemq queue---"+message);
        try {
            ComplaintMessage complaintMessage = objectMapper.readValue(message, ComplaintMessage.class);
            iComplaintEventHandler.handleCreateComplaintEvent(complaintMessage);

        } catch (JsonProcessingException e) {
            log.info("this object failed while objectMapper.readValue from this json object {}", message);
            log.error("consumeMessage -> objectMapper.readValue failed with this error", e);
        }
    }
}
