package com.intuit.complaint.management.rests;

import com.intuit.complaint.management.controller.ComplaintsManagementController;
import com.intuit.swagger.api.complaint.AddNewComplaintsApi;
import com.intuit.swagger.model.complaint.AddNewComplaintsRequest;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class AddNewComplaintsRest implements AddNewComplaintsApi {
    private ComplaintsManagementController complaintsManagementController;

    @Override
    public ResponseEntity<Void> addNewComplaints(
            @ApiParam(value = ""  )  @Valid @RequestBody AddNewComplaintsRequest addNewComplaintsRequest){
        checkMandatoryField(addNewComplaintsRequest.getUserId(), "userId");
        checkMandatoryField(addNewComplaintsRequest.getPurchaseId(), "purchaseId");
        checkMandatoryField(addNewComplaintsRequest.getComplaint(), "complaint");
        checkMandatoryField(addNewComplaintsRequest.getSubject(), "subject");

        complaintsManagementController.addNewComplaints(addNewComplaintsRequest);
        return ResponseEntity.noContent().build();
    }

    private void checkMandatoryField(String str, String fieldName) {
        if (str == null || "".equals(str)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Mandatory field is missing (" + fieldName + ")");
        }
    }
}
