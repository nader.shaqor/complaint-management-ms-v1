package com.intuit.complaint.management.rests;

import com.intuit.complaint.management.controller.ComplaintsManagementController;
import com.intuit.swagger.api.complaint.RetriveComplaintsApi;
import com.intuit.swagger.model.complaint.RetriveComplaintsResponse;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;

@RestController
@AllArgsConstructor
public class ComplaintsRest implements RetriveComplaintsApi {

    private ComplaintsManagementController complaintsManagementController;

    @Override
    public ResponseEntity<RetriveComplaintsResponse> retriveComplaints(
            @ApiParam(value = "ID of user") @Valid @RequestParam(value = "userId", required = false) String userId,
            @ApiParam(value = "ID of purchase") @Valid @RequestParam(value = "purchaseId", required = false) String purchaseId,
            @ApiParam(value = "Page Number to get.", defaultValue = "1") @Valid @RequestParam(value = "page", required = false, defaultValue="1") BigDecimal page,
            @ApiParam(value = "page size of items to return.", defaultValue = "10") @Valid @RequestParam(value = "size", required = false, defaultValue="10") BigDecimal size){

        return ResponseEntity.ok(complaintsManagementController.retrieveComplaints(userId,purchaseId,page,size));
    }
}
