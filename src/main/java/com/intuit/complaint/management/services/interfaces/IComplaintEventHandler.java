package com.intuit.complaint.management.services.interfaces;

import com.intuit.complaint.management.models.ComplaintMessage;

public interface IComplaintEventHandler {
    void handleCreateComplaintEvent(ComplaintMessage complaintMessage);
}
