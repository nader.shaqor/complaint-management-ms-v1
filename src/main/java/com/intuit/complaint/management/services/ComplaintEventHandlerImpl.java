package com.intuit.complaint.management.services;

import com.intuit.complaint.management.gateway.PurchasesServicesClient;
import com.intuit.complaint.management.gateway.UserManagementServicesClient;
import com.intuit.complaint.management.models.ComplaintMessage;
import com.intuit.complaint.management.models.PurchasesDetails;
import com.intuit.complaint.management.models.UserDetails;
import com.intuit.complaint.management.mongo.dao.ComplaintDao;
import com.intuit.complaint.management.mongo.models.ComplaintModel;
import com.intuit.complaint.management.mongo.models.extensions.PurchasesDetailsModel;
import com.intuit.complaint.management.mongo.models.extensions.UserDetailsModel;
import com.intuit.complaint.management.services.interfaces.IComplaintEventHandler;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Log4j2
@Service
@AllArgsConstructor
public class ComplaintEventHandlerImpl implements IComplaintEventHandler {
    private UserManagementServicesClient userManagementServicesClient;
    private PurchasesServicesClient purchasesServicesClient;
    private ComplaintDao complaintDao;


    @Override
    public void handleCreateComplaintEvent(ComplaintMessage complaintMessage) {
        UserDetails userDetails = userManagementServicesClient.getUserById(complaintMessage.getUserId());
        log.debug("handleCreateComplaintEvent -> userDetails = {}", userDetails);

        PurchasesDetails purchasesDetails = purchasesServicesClient.getPurchasesDetailsById(complaintMessage.getPurchaseId());
        log.debug("handleCreateComplaintEvent -> purchasesDetails = {}", purchasesDetails);


        ComplaintModel complaintModel = new ComplaintModel();

        populateComplantInfoFromMessage(complaintMessage, complaintModel);
        
        if(userDetails != null) {
            populateComplaintWithUserDetails(userDetails, complaintModel);
        }
        
        if(purchasesDetails !=null) {
            populateComplaintWithPurchasesDetails(purchasesDetails, complaintModel);
        }

        complaintModel.setCreatedAt(Instant.now());
        complaintModel.setUpdatedAt(Instant.now());

        complaintDao.insertNewComplaint(complaintModel);

    }

    private void populateComplaintWithPurchasesDetails(PurchasesDetails purchasesDetails, ComplaintModel complaintModel) {
        PurchasesDetailsModel purchasesDetailsModel = new PurchasesDetailsModel();
        purchasesDetailsModel.setId(purchasesDetails.getId());
        purchasesDetailsModel.setUserId(purchasesDetails.getUserId());
        purchasesDetailsModel.setProductId(purchasesDetails.getProductId());
        purchasesDetailsModel.setProductName(purchasesDetails.getProductName());
        purchasesDetailsModel.setPricePaidAmount(purchasesDetails.getPricePaidAmount());
        purchasesDetailsModel.setPriceCurrency(purchasesDetails.getPriceCurrency());
        purchasesDetailsModel.setDiscountPercent(purchasesDetails.getDiscountPercent());
        purchasesDetailsModel.setMerchantId(purchasesDetails.getMerchantId());
        if(purchasesDetails.getPurchaseDate() != null) {
            purchasesDetailsModel.setPurchaseDate(purchasesDetails.getPurchaseDate().toInstant());
        }


        complaintModel.setPurchasesDetailsData(purchasesDetailsModel);
    }

    private void populateComplaintWithUserDetails(UserDetails userDetails, ComplaintModel complaintModel) {
        UserDetailsModel userDetailsModel = new UserDetailsModel();
        userDetailsModel.setId(userDetails.getId());
        userDetailsModel.setFullName(userDetails.getFullName());
        userDetailsModel.setEmailAddress(userDetails.getEmailAddress());
        userDetailsModel.setPhysicalAddress(userDetails.getPhysicalAddress());
        complaintModel.setUserDetailsData(userDetailsModel);
    }

    private void populateComplantInfoFromMessage(ComplaintMessage complaintMessage, ComplaintModel complaintModel) {
        complaintModel.setUserId(complaintMessage.getUserId());
        complaintModel.setSubject(complaintMessage.getSubject());
        complaintModel.setComplaint(complaintMessage.getComplaint());
        complaintModel.setPurchaseId(complaintMessage.getPurchaseId());
    }
}
