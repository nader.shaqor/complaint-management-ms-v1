package com.intuit.complaint.management.gateway;

import com.intuit.complaint.management.gateway.interfaces.IPurchasesServicesClient;
import com.intuit.complaint.management.models.PurchasesDetails;
import com.intuit.complaint.management.property.PurchasesClientApiProperties;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
@Service
@Log4j2
public class PurchasesServicesClient implements IPurchasesServicesClient {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PurchasesClientApiProperties clientApiProperties;

    @Override
    public PurchasesDetails getPurchasesDetailsById(String purchasesId) {
        log.debug("getPurchasesDetailsById called with purchasesId {}", purchasesId);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String apiUrl = clientApiProperties.getApiHost() + clientApiProperties.getApiPath().replace("{purchaseId}", purchasesId);
        try {
            return restTemplate.exchange(apiUrl, HttpMethod.GET, entity, PurchasesDetails.class).getBody();
        }catch (RestClientException e){
            log.error("failed while calling apiUrl with this error", e);
            return null;
        }
    }
}
