package com.intuit.complaint.management.gateway;

import com.intuit.complaint.management.gateway.interfaces.IUserManagementServicesClient;
import com.intuit.complaint.management.models.UserDetails;
import com.intuit.complaint.management.property.UserClientApiProperties;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
@Log4j2
public class UserManagementServicesClient implements IUserManagementServicesClient {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserClientApiProperties clientApiProperties;

    public UserDetails getUserById(String userId) {
        log.debug("getUserById called with userId {}", userId);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String apiUrl = clientApiProperties.getApiHost() + clientApiProperties.getApiPath().replace("{userId}", userId);
        try {
            return restTemplate.exchange(apiUrl, HttpMethod.GET, entity, UserDetails.class).getBody();
        } catch (RestClientException e) {
            log.error("failed while calling apiUrl with this error", e);
            return null;
        }
    }

}
