package com.intuit.complaint.management.gateway.interfaces;

import com.intuit.complaint.management.models.PurchasesDetails;

public interface IPurchasesServicesClient {
    PurchasesDetails getPurchasesDetailsById(String purchasesId);
}
