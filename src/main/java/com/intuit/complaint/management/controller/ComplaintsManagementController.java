package com.intuit.complaint.management.controller;

import com.intuit.complaint.management.models.ComplaintMessage;
import com.intuit.complaint.management.mongo.dao.ComplaintDao;
import com.intuit.complaint.management.mongo.models.ComplaintModel;
import com.intuit.complaint.management.mongo.models.extensions.PurchasesDetailsModel;
import com.intuit.complaint.management.mongo.models.extensions.UserDetailsModel;
import com.intuit.complaint.management.services.interfaces.IComplaintEventHandler;
import com.intuit.swagger.model.complaint.*;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Controller
@AllArgsConstructor
public class ComplaintsManagementController {

    private ComplaintDao complaintDao;
    private IComplaintEventHandler complaintEventHandler;

    public void addNewComplaints(AddNewComplaintsRequest addNewComplaintsRequest) {
        ComplaintMessage complaintMessage = new ComplaintMessage();
        complaintMessage.setUserId(addNewComplaintsRequest.getUserId());
        complaintMessage.setSubject(addNewComplaintsRequest.getSubject());
        complaintMessage.setComplaint(addNewComplaintsRequest.getComplaint());
        complaintMessage.setPurchaseId(addNewComplaintsRequest.getPurchaseId());
        complaintEventHandler.handleCreateComplaintEvent(complaintMessage);
    }

    public RetriveComplaintsResponse retrieveComplaints(String userId, String purchaseId, BigDecimal page, BigDecimal size) {
        log.debug("retrieveComplaints called with userId {}, purchaseId {} , page {} and size {}");
        Page<ComplaintModel> complaintModelPage = complaintDao.getComplaint(userId,purchaseId, page.intValue()-1,size.intValue());

        if (!complaintModelPage.hasContent()) {
            log.debug("retrieveComplaints not found any complaint for userId {}, purchaseId {} , page {} and size {}");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No complaints ware found");
        }

        RetriveComplaintsResponse retriveComplaintsResponse = new RetriveComplaintsResponse();

        List<ComplaintDetails> complaintDetailsList = complaintModelPage.stream().map(complaintModel -> {
            return getComplaintDetails(complaintModel);
        }).collect(Collectors.toList());

        retriveComplaintsResponse.setData(complaintDetailsList);
        retriveComplaintsResponse.setPageNumber(complaintModelPage.getNumber()+1);
        retriveComplaintsResponse.setPageSize(complaintModelPage.getSize());
        retriveComplaintsResponse.setTotalElements((int)complaintModelPage.getTotalElements());
        return retriveComplaintsResponse;
    }

    private ComplaintDetails getComplaintDetails(ComplaintModel complaintModel) {
        ComplaintDetails complaintDetails = new ComplaintDetails();
        complaintDetails.setComplaintid(complaintModel.getId());
        complaintDetails.setUserId(complaintModel.getUserId());
        complaintDetails.setPurchaseId(complaintModel.getPurchaseId());
        complaintDetails.setSubject(complaintModel.getSubject());
        complaintDetails.setComplaint(complaintModel.getComplaint());
        UserDetailsModel userDetailsModel = complaintModel.getUserDetailsData();
        if(userDetailsModel != null) {
            UserDetails userDetails = new UserDetails();
            userDetails.setUserId(userDetailsModel.getId());
            userDetails.setFullName(userDetailsModel.getFullName());
            userDetails.setEmailAddress(userDetailsModel.getEmailAddress());
            userDetails.setPhysicalAddress(userDetailsModel.getPhysicalAddress());
            complaintDetails.setUserDetails(userDetails);
        }
        PurchasesDetailsModel purchasesDetailsModel = complaintModel.getPurchasesDetailsData();
        if(purchasesDetailsModel != null){
            PurchasesDetails purchasesDetails = new PurchasesDetails();
            purchasesDetails.setPurchaseId(purchasesDetailsModel.getId());
            purchasesDetails.setUserId(purchasesDetailsModel.getUserId());
            purchasesDetails.setProductId(purchasesDetailsModel.getProductId());
            purchasesDetails.setProductName(purchasesDetailsModel.getProductName());
            purchasesDetails.setPricePaidAmount(purchasesDetailsModel.getPricePaidAmount());
            purchasesDetails.setPriceCurrency(purchasesDetailsModel.getPriceCurrency());
            purchasesDetails.setDiscountPercent(purchasesDetailsModel.getDiscountPercent());
            purchasesDetails.setMerchantId(purchasesDetailsModel.getMerchantId());
            if(purchasesDetailsModel.getPurchaseDate() != null) {
                purchasesDetails.setPurchaseDate(purchasesDetailsModel.getPurchaseDate().atOffset(ZoneOffset.UTC));
            }
            complaintDetails.setPurchasesDetails(purchasesDetails);
        }
        return complaintDetails;
    }
}
